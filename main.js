var strona, dziennik;
$(document).ready(function() {
  console.log("Wczytano jquery");
  $("#dodaj_ucznia_div").hide();
  $("#dodaj_ucznia_button").on("click", dodaj_ucznia);
  $("#zapisz_ucznia_button").on("click", zapisz_ucznia);
  $("#oceny_button").on("click", pobierz_dane);
});

function pobierz_dane() {
  console.log("funkcja pobierz_dane");
  $.ajax({
    asoc: "false",
    url: "dziennik.json",
    method: "post",
    success: function(res) {
      dziennik = res["dziennik"];
      console.log("pobrano dane");
      wyswietl_osoby();
    },
    error: function(res) {
      console.error(res);
    }
  })
}

function wyswietl_osoby() {
  console.log("Funkcja wyswietl_osoby");
  console.log(dziennik);
  strona = "oceny";
  $("#oceny_div").show();
  $("#oceny_div").append("<label>Wybierz ucznia<select id = 'osoby'></select></label>");
  dziennik.forEach(klucz => $("#osoby").append("<option data-imie='" + klucz["imie"] + "' data-nazwisko='" + klucz["nazwisko"] + "'>" + klucz["imie"] + " " + klucz["nazwisko"] + "</option>"));
  $(document).on("change", "#osoby", function() {
    wyswietl_oceny($("#osoby option:selected").data("imie"), $("#osoby option:selected").data("nazwisko"));
  });
}

function wyswietl_oceny(imie, nazwisko) {
  console.log("Funkcja wyswietl_oceny");
  dziennik.forEach(function(klucz) {
    if((klucz["imie"] == imie)&& (klucz["nazwisko"] == nazwisko))  {
      $("#oceny_div").append("<div><label>Polski<input type = 'text' class = 'aktualne_oceny' value = '" + klucz["oceny"][0] + "'></label></div>");
      $("#oceny_div").append("<div><label>Angielski<input type = 'text' class = 'aktualne_oceny' value = '" + klucz["oceny"][1] + "'></label></div>");
      $("#oceny_div").append("<div><label>Religia<input type = 'text' class = 'aktualne_oceny' value = '" + klucz["oceny"][2] + "'></label></div>");
      $("#oceny_div").append("<div><button id = 'zapisz_oceny_button'>Zmien oceny</button>");
      $(document).on("click", "#zapisz_oceny_button", function() {zapisz_oceny(imie, nazwisko)});
    }
  });
}

function zapisz_oceny(imie, nazwisko) {
  console.log("Funkcja zapisz_oceny");
  json = {
    imie: imie,
    nazwisko: nazwisko,
    oceny: [
      $(".aktualne_oceny:eq(0)").val(),
      $(".aktualne_oceny:eq(1)").val(),
      $(".aktualne_oceny:eq(2)").val()
    ]
  }
  $.ajax({
    url: "zmien_oceny.php",
    method: "post",
    data: json
  })
  .fail(res => {
    console.error("Nie udało się zmienić ocen");
    console.error(res);
  })
  .done(res => {
    console.log("Oceny zmienione");
    console.log(res);
  })
}

function dodaj_ucznia() {
  console.log("Funkcja dodaj_ucznia");
  if(strona == "dodaj_ucznia") $("#dodaj_ucznia_div").hide();
  else {
    $("#dodaj_ucznia_div").show();
    strona = "dodaj_ucznia";
  }
}

function zapisz_ucznia() {
  console.log("zapisz_ucznia");
  json = {
    imie: $("#imie").val(),
    nazwisko: $("#nazwisko").val(),
    oceny: [
      {"Polski": "0"},
      {"Angielski": "0"},
      {"Religia": "0"}
    ]
  }
  console.log(json);
  $("#imie").val("");
  $("#nazwisko").val("");
  $.ajax({
    url: "zapisz.php",
    method: "post",
    data: json,
    success: function(res) {
      console.log("dodano ucznia");
      console.log(res);
    },
    error: function(res) {
      console.error(res);
    }
  });
}
